dictionnaire = {
    "key0": "value0",
    "key1": "value1",
    "key2": "value2",
    "key3": "value3"
    }

#################################################################
#################################################################

# Supprimer une clé du dictionnaire
valeur_supprimee = dictionnaire.pop("key3")

#################################################################
#################################################################

# Lire les clés dans le dictionnaire
for key in dictionnaire.keys():
    print(key)

# Lire les valeurs dans le dictionnaire
for value in dictionnaire.values():
    print(value)

# Lire les clés et les valeurs dans le dictionnaire
for key,value in dictionnaire.items():
    print(f"key: {key} | value: {value}")

#################################################################
#################################################################

# Ajouter clés dans le dictionnaire
dictionnaire["key4"] = "value4"

#################################################################
#################################################################

# Creer une fonction qui return un dictionnaire
def CreationDictionnaire(**parameters):
    print(parameters)

dico = CreationDictionnaire(lapin="mignon", mustang=75000, catapulte="tabouret")

print(dico)