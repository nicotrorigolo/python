class Moteur:
    def __init__(self, type: str, constructeur: str):
        self.type = type
        self.contructeur = constructeur

    def caracteristique_moteur(self):
        print(f"Type: {self.type}\nConstructeur: {self.contructeur}")

# Une classe ne peut herité que d'une classe
# ici, la classe "Voiture" hérite de la classe "Moteur"
# La classe "Moteur" est la classe Parent de la classe "Voiture"
class Voiture(Moteur):
    def __init__(self, type: str, constructeur: str, modele: str):
        # On utilise "super" pour pouvoir appeler des elements de la classe parent
        super().__init__(type, constructeur)
        self.modele = modele

    def ModeleVoiture(self):
        print(f"Modele de voiture: {self.modele}")

    def ConstructeurVoiture(self):
        print(f"Constructeur de voiture: {self.contructeur}")

    def SyntheseVoiture(self):
        self.ModeleVoiture()
        super().caracteristique_moteur()
        

voiture = Voiture("Electrique", "Ford", "Mustang")
voiture.SyntheseVoiture()