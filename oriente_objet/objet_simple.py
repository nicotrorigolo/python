class Personne:
# Dans une class les variables qui ne sont pas mis dans le constructeur
# sont appellé : variable de classe

# La variable de classe est partagé avec tous les objets
    planete = "terre"

# ": str" ou ": int",
# donne une indication sur comment renseigner les arguments de l'objet,
# cette ajout n'impacte en rien la fonctionnalité du code

# Les variables de la fonction '__init__' sont appellé variable d'instance

#'nom: str = ""' indique que la valeur par defaut est "", donc vide 
    def __init__(self, nom: str = "", prenom: str = "", age: int = 70, permis = True):
        self.nom = nom          
        self.prenom = prenom
        self.age = age
        self.permis = permis

        if self.nom == "":
            self.DemanderNom()

    def SePresenter(self):
        print(f"Salut, je suis {self.nom} !")
        # Grace au "self." on peut appeler une methode présente dans l'objet
        self.PossederLePermis()

    def DonnerSonAge(self):
        print(f"J'ai {self.age} ans !")

    def PossederLePermis(self):
        if self.permis == True:
            print(f"Oui, j'ai mon permis !")
        else:
            print(f"Non, je n'ai pas mon permis !")

    # Cette methode va permettre de modifier les variables d'instance de l'objet
    def DemanderNom(self):
        self.nom = input("Demander nom:")

    # Cet ecriture de methode est appellé : Fonction de classe ou Fonction statique
    def DonnerSaPlanete():
        print(f"J'habite sur la {Personne.planete}")

personne1 = Personne("lapin", "tabouret", 50, True)
personne2 = Personne("nico", "catapulte", 38, False)
personne3 = Personne()

personne1.SePresenter()
personne2.SePresenter()
personne3.SePresenter()

#################################################################
#################################################################

# On peut aussi appeler un des parametre de l'objet
print(personne1.age)

#################################################################
#################################################################

# On peut aussi avoir des objets dans une liste
liste_personne = [
    Personne("coquelicot", "vache", 40, True), 
    Personne("haricot", "poule", 80, False)
    ]
liste_personne[0].SePresenter()
liste_personne[1].SePresenter()

#################################################################
#################################################################

# Fonction de classe
Personne.DonnerSaPlanete()
